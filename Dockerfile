FROM python:3.10-slim-bullseye

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y build-essential python3-dev && \
    apt-get clean -y && \
    apt-get autoclean -y

COPY requirements.txt ./

RUN pip3 install --no-cache-dir -r requirements.txt

WORKDIR /app

COPY . .

EXPOSE 5000

ENTRYPOINT gunicorn -b 0.0.0.0:5000 appzcs.wsgi:app