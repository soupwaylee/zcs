import unittest
from appzcs.utils import find_first_shortest_sequence, clean_text, count_connected_ones_components, assemble_matrix


class TestFindShortestSequence(unittest.TestCase):
    def test_should_clean_correctly(self):
        # GIVEN
        text = "Apples, Wörter, Pears and Bananas!"

        # WHEN
        actual = clean_text(text)

        # THEN
        expected = "Apples Wörter Pears and Bananas"
        self.assertEqual(expected, actual)

    def test_should_not_find_first_shortest_sequence_when_text_is_too_short(self):
        # GIVEN
        text = "two words"
        target_sequence = set("three words here".split())
        k = 3

        # WHEN
        actual = find_first_shortest_sequence(text, k, target_sequence)

        # THEN
        expected = []
        self.assertEqual(expected, actual)

    def test_should_find_first_shortest_sequence(self):
        # GIVEN
        text = ("Ein toller Beispieltext ist der Blindtext. Er hat ein paar Wörter. Dies ist\n"
                "ein Beispieltext, der ein paar Wörter hat und auch noch ein paar mehr, um\n"
                "die Zeile etwas länger zu machen. Darüber hinaus ist er nur dafür da, um\n"
                "genügend Testtext zusammenzubekommen. Dem Text selbst macht das nicht so\n"
                "viel aus. Früher einmal mehr, als er noch nicht so selbstbewusst war. Heute\n"
                "kennt er seine Rolle als Blindtext und fügt sich selbstbewusst ein. Er ist\n"
                "ja irgendwie wichtig. Manchmal jedoch, ganz manchmal, weint er in der\n"
                "Nacht, weil er niemals bis zum Ende gelesen wird. Doch das hat ja jetzt zum\n"
                "Glück ein Ende.")
        target_sequence = set("Beispieltext der ein paar Wörter".split())
        k = 5

        # WHEN
        actual = find_first_shortest_sequence(text, k, target_sequence)

        # THEN
        expected = "Beispieltext der ein paar Wörter".split()
        self.assertEqual(expected, actual)

    def test_should_find_no_sequence_if_it_is_not_present(self):
        # GIVEN
        text = "Ein paar Wörter"
        target_sequence = set("Zwei paar Wörter".split())
        k = 3

        # WHEN
        actual = find_first_shortest_sequence(text, k, target_sequence)

        # THEN
        expected = []
        self.assertEqual(expected, actual)

    def test_should_find_sequence_at_end(self):
        # GIVEN
        text = "Ein paar Wörter"
        target_sequence = set("Wörter".split())
        k = 1

        # WHEN
        actual = find_first_shortest_sequence(text, k, target_sequence)

        # THEN
        expected = ["Wörter"]
        self.assertEqual(expected, actual)


class TestCountConnectedComponents(unittest.TestCase):
    def test_should_assemble_matrix_correctly(self):
        # GIVEN
        bytes = [
            b"0 0 1 0",
            b"1 0 1 0",
            b"0 1 0 0",
            b"1 1 1 1",
        ]
        dimension = 4

        # WHEN
        actual = assemble_matrix(bytes, dimension)

        # THEN
        expected = [
            [0, 0, 1, 0],
            [1, 0, 1, 0],
            [0, 1, 0, 0],
            [1, 1, 1, 1]
        ]
        self.assertEqual(expected, actual)

    def test_should_not_find_any_components(self):
        # GIVEN
        matrix = [
            [0]
        ]

        # WHEN
        actual = count_connected_ones_components(matrix)

        # THEN
        expected = 1
        self.assertEqual(expected, actual)

    def test_should_find_component(self):
        # GIVEN
        matrix = [
            [1, 0, 0, 1, 1],
            [0, 0, 1, 0, 0],
            [0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0]
        ]

        # WHEN
        actual = count_connected_ones_components(matrix)

        # THEN
        expected = 3
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()