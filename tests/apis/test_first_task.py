import unittest

from appzcs.__init__ import create_app

SAMPLE_TEXT = """Ein toller Beispieltext ist der Blindtext. Er hat ein paar Wörter. Dies ist
ein Beispieltext, der ein paar Wörter hat und auch noch ein paar mehr, um
die Zeile etwas länger zu machen. Darüber hinaus ist er nur dafür da, um
genügend Testtext zusammenzubekommen. Dem Text selbst macht das nicht so
viel aus. Früher einmal mehr, als er noch nicht so selbstbewusst war. Heute
kennt er seine Rolle als Blindtext und fügt sich selbstbewusst ein. Er ist
ja irgendwie wichtig. Manchmal jedoch, ganz manchmal, weint er in der
Nacht, weil er niemals bis zum Ende gelesen wird. Doch das hat ja jetzt zum
Glück ein Ende.
"""


class FirstTaskTestCases(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.config.update({
            "TESTING": True,
        })
        self.client = self.app.test_client

    def test_should_return_first_shortest_sequence(self):
        # GIVEN
        task_data = f"""{SAMPLE_TEXT}
5
ein
Beispieltext
der
paar
Wörter
"""

        # WHEN
        res = self.client().post(
            '/first-task',
            data=task_data
        )

        # THEN
        self.assertEqual(res.status_code, 200)
        self.assertIn("Beispieltext der ein paar Wörter", res.data.decode('utf-8'))

    def test_should_return_sequence_not_found(self):
        # GIVEN
        task_data = f"""{SAMPLE_TEXT}
5
diese
Wörter
kommen
nicht
vor
"""

        # WHEN
        res = self.client().post(
            '/first-task',
            data=task_data
        )

        # THEN
        self.assertEqual(res.status_code, 200)
        self.assertIn("KEIN ABSCHNITT GEFUNDEN", res.data.decode('utf-8'))