import unittest

from appzcs.__init__ import create_app


class SecondTaskTestCases(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.config.update({
            "TESTING": True,
        })
        self.client = self.app.test_client

    def test_should_return_ones_components_correctly(self):
        # GIVEN
        task_data = """4
4
0 0 1 0
1 0 1 0
0 1 0 0
1 1 1 1
4
1 0 0 1
0 0 0 0
0 1 1 0
1 0 0 1
5
1 0 0 1 1
0 0 1 0 0
0 0 0 0 0
1 1 1 1 1
0 0 0 0 0
8
0 0 1 0 0 1 0 0
1 0 0 0 0 0 0 1
0 0 1 0 0 1 0 1
0 1 0 0 0 1 0 0
1 0 0 0 0 0 0 0
0 0 1 1 0 1 1 0
1 0 1 1 0 1 1 0
0 0 0 0 0 0 0 0
"""

        # WHEN
        res = self.client().post(
            '/second-task',
            data=task_data
        )

        # THEN
        self.assertEqual(200, res.status_code)
        self.assertIn("1\n3\n3\n9", res.data.decode('utf-8'))
