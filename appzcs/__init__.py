import os
from flask import Flask

from appzcs.apis.first_task import first_task_blueprint
from appzcs.apis.second_task import second_task_blueprint


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev'
    )

    env_config = os.getenv("APP_MODE", "dev")

    if env_config == "dev":
        app.config.update(
            TESTING=True,
            DEBUG=True
        )
    elif env_config == "prod":
        app.config.update(
            DEBUG=False,
            DEVELOPMENT=False
        )

    if test_config is not None:
    #     app.config.from_pyfile('config.py', silent=True)
    # else:
        app.config.from_mapping(test_config)

    app.register_blueprint(first_task_blueprint)
    app.register_blueprint(second_task_blueprint)

    return app
