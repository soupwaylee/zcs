from flask import abort, Blueprint, request
from appzcs.utils import assemble_matrix, count_connected_ones_components

second_task_blueprint = Blueprint('second_task', __name__)


@second_task_blueprint.route('/second-task', methods=['POST'])
def second_task():
    request_bytes = request.data.splitlines()
    testcases = 0
    matrix_list = []

    for i, line in enumerate(request_bytes):
        if i == 0:
            if line.isdigit(): # strip trailing whitespace
                testcases = int(line)
                if testcases not in range(1, 6): # 0 < T < 6
                    print(f"T = {testcases}")
                    abort(413)
            else:
                abort(400)
        else:
            if line.isdigit():
                dimension = int(line)
                if dimension not in range(1, 1009): # 0 < N < 1009
                    abort(413)
                try:
                    matrix = assemble_matrix(request_bytes[i+1:i+1+dimension], dimension)
                    matrix_list.append(matrix)
                except (RuntimeError, IndexError):
                    abort(400)

    result = [str(count_connected_ones_components(matrix)) for matrix in matrix_list]
    result_formatted = "\n".join(result)

    return f"""
<div id='solution'>
    <pre>
        {result_formatted}
    </pre>
</div>
    """

