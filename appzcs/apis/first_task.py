from flask import abort, Blueprint, request
from appzcs.utils import find_first_shortest_sequence
from io import StringIO

first_task_blueprint = Blueprint('first_task', __name__)


@first_task_blueprint.route('/first-task', methods=['POST'])
def first_task():
    if request.content_length > 200000:
        abort(413)

    text_data = StringIO()
    k = 0
    target_word_set = set()
    is_target_word_set = False
    for line in request.data.splitlines():
        if line.isdigit():
            k = int(line)
            is_target_word_set = True
        else:
            if is_target_word_set:
                target_word_set.add(line.decode())
            else:
                text_data.write(line.decode())
    text = text_data.getvalue()

    result = find_first_shortest_sequence(text, k, target_word_set)

    response_html = f"""
<div id='solution'>
    <pre>
        {" ".join(result) if result else "KEIN ABSCHNITT GEFUNDEN"}
    </pre>
</div>
    """

    return response_html


