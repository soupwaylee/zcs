import re
from collections import defaultdict, deque


def find_first_shortest_sequence(text: str, k: int, target_word_set: set[str]) -> list[str]:
    text_list = clean_text(text).split()

    if len(text_list) < k:
        return []

    target_word_set_lower = {word.lower() for word in target_word_set}

    first_shortest_sequence = []
    target_word_occurrences = defaultdict(lambda: 0)
    start = 0
    distinct_count = 0
    shortest_sequence_len = len(text_list)

    for end in range(len(text_list)):
        current_word = text_list[end].lower()

        # seen one of the words
        if current_word in target_word_set_lower:
            target_word_occurrences[current_word] += 1

        # first time seeing it?
        if target_word_occurrences[current_word] == 1:
            distinct_count += 1

        # we've seen all words now
        if distinct_count == k:
            while (target_word_occurrences[text_list[start].lower()] > 1
                   or not text_list[start].lower() in target_word_set_lower):
                # word at start occurs multiple times --> not distinct
                if target_word_occurrences[text_list[start].lower()] > 1:
                    target_word_occurrences[text_list[start].lower()] -= 1
                start += 1

            interval_len = end - start + 1

            if interval_len < shortest_sequence_len:
                shortest_sequence_len = interval_len
                first_shortest_sequence = text_list[start:start + shortest_sequence_len]

    return first_shortest_sequence


def clean_text(text: str) -> str:
    return re.sub(r'[^a-zA-Z\säüöÄÜÖ]', '', text)


def count_connected_ones_components(matrix: list[list[int]]) -> int:
    rows, cols = len(matrix), len(matrix[0])
    components = 0
    visited_coordinates = set()

    neighbors = [
        (-1, -1), (-1, 0), (-1, 1),
        (0, -1), (0, 1),
        (1, -1), (1, 0), (1, 1)
    ]

    def breadth_first_search(r, c, entry):
        queue = deque()
        visited_coordinates.add((r, c))
        queue.append((r, c))

        while len(queue) > 0:
            row, col = queue.popleft()
            for r_diff, c_diff in neighbors:
                if ((row + r_diff) in range(rows)
                        and (col + c_diff) in range(cols)
                        and matrix[row + r_diff][col + c_diff] == entry
                        and (row + r_diff, col + c_diff) not in visited_coordinates):
                    queue.append((row + r_diff, col + c_diff))
                    visited_coordinates.add((row + r_diff, col + c_diff))

    for r in range(rows):
        for c in range(cols):
            if matrix[r][c] == 1 and (r, c) not in visited_coordinates:
                breadth_first_search(r, c, 1)
                components += 1

    return components


def assemble_matrix(bytes_list: list[bytes], dimension: int) -> list[list[int]]:
    matrix = []

    for i, line in enumerate(bytes_list):
        row = [int(elt) for elt in line.split()]
        if len(row) != dimension:
            raise RuntimeError(f"Expected: {dimension} columns, Got: {len(row)}")
        matrix.append(row)

    return matrix
